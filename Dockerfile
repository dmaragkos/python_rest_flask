# Use an official Python runtime as a parent image
FROM python:2.7-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 3000

# Run app.py when the container launches
CMD ["python", "server.py"]
