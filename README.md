# Deploy on AWS

- aws configure
- $(aws ecr get-login --no-include-email --region eu-central-1)
- docker build -t python_rest_flask .
- docker tag python_rest_flask:latest 692868696977.dkr.ecr.eu-central-1.amazonaws.com/python_rest_flask:latest
- docker push 692868696977.dkr.ecr.eu-central-1.amazonaws.com/python_rest_flask:latest
- create a task definition, a cluster and a service on Amazon ECS
- Add Auto-configure Cloudwatch logs in container definition
- ssh -i ~/Coding/aws-keypair/dmaragkos.pem ec2-user@ec2-3-120-32-25.eu-central-1.compute.amazonaws.com