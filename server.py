#!/usr/bin/python3
from flask import Flask
from flask_restful import Resource, Api
from sqlalchemy import create_engine

db_connect = create_engine('sqlite:///chinook.db')
app = Flask(__name__)
api = Api(app)


class Employees(Resource):
    def get(self):
        conn = db_connect.connect() # connect to database
        query = conn.execute("select * from employees") # This line performs query and returns json result
        return {'employees': [i[0] for i in query.cursor.fetchall()]} # Fetches first column that is Employee ID

    def post(self):
        return {'status': 'success'}


api.add_resource(Employees, '/employees')  # Route_1

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)
